(define-module (terramorpha libs package-utils)
  #:use-module (guix packages)
  #:use-module (guix store)
  #:export (origin-file-dumper
			builder-copy-source))

(define* (origin-file-dumper content #:key (file-name "file"))
  (define* (method url hash-algo hash
				   #:optional name
				   #:key (system (%current-system))
				   (guile (default-guile)))
	(text-file file-name content))
  (origin
   (uri "whatever")
   (method method)
   (sha256 (base32 "0000000000000000000000000000000000000000000000000000"))))

(define* (builder-copy-source dst #:optional executable?)
  `(begin
	 (use-modules (guix build utils)) ; for mkdir-p
	 (let* ((dst ,dst)
			(src (assoc-ref %build-inputs "source"))
			(full-dst (string-append %output dst)))
	   (mkdir-p (dirname full-dst))
	   (copy-file src full-dst)
	   ,@(if executable?
			 '((chmod dst #o755))
			 '()))))

(define-module (terramorpha libs string-interpolation)
  #:export (string-interpolate
			string-extract-interpolation
			just))

(use-modules (ice-9 exceptions)
			 (ice-9 pretty-print))

(define just identity)

(define (string-extract-interpolation str)
  (with-input-from-string str
	(lambda ()
	  (define escaped #f)
	  (define strings #nil)
	  (define current-string "")
	  (define (seek-char)
		(define c (read-char))
		(unread-char c)
		c)
	  (define (flush-current-string)
		(set! strings (append strings (list current-string)))
		(set! current-string ""))
	  (define (add-to-current-string s/c)
		(cond
		 ((char? s/c)
		  (set! current-string (string-append current-string (string s/c))))
		 ((string? s/c)
		  (set! current-string (string-append current-string s/c)))))
	  (define (loop)
		(let ((char (read-char)))
		  (unless (eof-object? char)
			(cond (escaped				; last char was a \
				   (add-to-current-string char)
				   (set! escaped #f))
				  ((eq? char #\#)		; char is a #
				   (if (eq? (seek-char) #\()
					   (let ((val (read)))
						 (flush-current-string)
						 (set! strings (append strings (list (car val)))))
					   (add-to-current-string char)))
				  ((eq? char #\\)		; char is a \
				   (set! escaped #t))
				  (else
				   (add-to-current-string char))) ; default -> append the char
			(loop))))
	  (loop)
	  (set! strings (append strings (list current-string)))
	  strings)))

(define-syntax string-interpolate
  (lambda (s)
	(let ((str (cadr (syntax->datum s))))
	  (unless (string? str)
		(raise-exception (make-exception-with-message "string-interpolate needs a string")))
	  ;; (pretty-print "/asdfasdf")
	  (let ((parsed (string-extract-interpolation str)))
		;; (pretty-print "parsed:")
		#`(string-append #,@(map (lambda (v)
								   (cond
									((string? v)
									 (datum->syntax s v))
									(else
									 (datum->syntax s `(format #f "~a" ,v)))))
								 parsed))))))




(define (pound-interpolate char stream)
  (let ((s (read stream)))
	`(string-interpolate ,s)))

(read-hash-extend #\< pound-interpolate)

;; (define l (string-extract-interpolation "salut, je m'appelle (+ 1 2)"))
;; (for-each (lambda (v)
;; 			(display v) (newline)) l)
;; (define b 1233)
;; #< "marmelade"
;; (display #i"salut (+ b 1)") (newline)
;; (pretty-print (string-extract-interpolation "salut (+ 1 2) mon beau"))
;; (pretty-print (string-interpolate "salut (+ 1 2) mon beau"))

;; (string-interpolate "salut mon beau (+ 1 2 3)")

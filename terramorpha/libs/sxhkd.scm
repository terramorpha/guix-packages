(define-module (terramorpha libs sxhkd)
  #:use-module (terramorpha libs string-interpolation)
  #:use-module (ice-9 exceptions)
  #:export (entries->string
			entry->string
			binding->string))

;; (define-syntax ++
;;   (lambda (s)
;; 	#`'(++ #,@(datum->syntax s (cdr (syntax->datum s))))))
;; (define-syntax :
;;   (lambda (s)
;; 	#`'(: #,@(datum->syntax s (cdr (syntax->datum s))))))


;; the valid modifiers are super, hyper, meta, alt, control, ctrl, shift,
;; mode_switch, lock, mod1, mod2, mod3, mod4, mod5 and any.
(define dangerous-characters '(#\! #\#))
(define (string-shell-escape str)
  (define ostring "")
  (let loop ((str (string->list str)))
	(if (null? str)
		ostring
		(if (member (car str) dangerous-characters)
			(begin
			  (set! ostring (string-append ostring (string #\\ (car str))))
			  (loop (cdr str)))
			(begin
			  (set! ostring (string-append ostring (string (car str))))
			  (loop (cdr str)))))))

(define (binding->string l)
  (cond
   ((list? l)
	(cond
	 ((eq? (car l) '+)
	  (string-join (map (lambda (v) (format #f "~a" v)) (cdr l)) " + "))
	 ((eq? (car l) ':)
	  (string-join (map binding->string (cdr l)) ";"))
	 (else
	  (raise-exception (make-exception-with-message "error: car of keybind must be + or :")))))
   ((symbol? l)
	(symbol->string l))
   (else
	(raise-exception (make-exception-with-message "error")))))

(define (entry->string e)
  (string-join
   (map (lambda (v) (string-append v "\n"))
		(list (binding->string (car e))
			   (string-append "\t" (string-join (map string-shell-escape (cdr e)) ";"))))
   ""))

(define (entries->string entries)
  (string-join
   (map entry->string entries)
   ""))



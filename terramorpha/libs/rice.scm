#!curly-infix
(define-module (terramorpha libs rice)
  #:use-module (terramorpha libs rice keybindings)
  #:use-module (gnu)
  #:export (desktop-entry->package
			i3-with-hardcoded-config
			xsession-entry))

(use-modules (ice-9 match)
			 (guix packages)
			 (guix gexp)
			 (guix store)
			 (guix download)
			 (guix build-system trivial)
			 ((guix licenses) #:prefix license:)
			 (gnu packages base)
			 (gnu packages wm)
			 (gnu packages bash)
			 (gnu packages aidc)
			 (gnu packages image-viewers)
			 (gnu packages linux)
			 (gnu packages compton))

(use-modules (terramorpha libs package-utils)
			 (terramorpha libs sxhkd)
			 (terramorpha libs string-interpolation))

(use-modules (srfi srfi-1))


(define* (xsession-entry name contents
						 #:key
						 (synopsis "xsession-entry")
						 (description "xsession-entry") 
						 (home-page "xsession-entry") 
						 (license license:non-copyleft) 
						 (version "xsession-entry"))

  (package
   (synopsis synopsis)
   (description description)
   (home-page home-page)
   (license license)
   (name name)
   (version version)
   (build-system trivial-build-system)
   (source (plain-file "empty" ""))
   (arguments
	`(#:modules ((guix build utils))
	  #:builder (begin
				  (use-modules (guix build utils))
				  (mkdir-p (string-append %output "/share/xsessions"))
				  (with-output-to-file (string-append %output "/share/xsessions/" ,name ".desktop")
					(lambda ()
					  (display "[Desktop Entry]\n")
					  (format (current-output-port) "Name=~a\n" ,name)
					  (format (current-output-port) "Exec=~a\n" (assoc-ref %build-inputs "startup-script"))
					  (format (current-output-port) "Type=Application\n")
					  (format (current-output-port) "X-LightDM-DesktopName=~a\n" ,name)
					  (format (current-output-port) "DesktopNames=~a\n" ,name))))))
   (inputs `(("startup-script" ,contents)))))

(define-public rice-wallpaper
  (origin
   (file-name "voyager.svg")
   (method url-fetch)
   (uri
	"https://ipfs.io/ipfs/QmV249CBCEM5AtwRhQZzLLVULXqTWdQDck7E3QGmzXFYTG")
   (sha256
    (base32
	 "0jy7iayci1cdkzs0h32n6kkwjm749kly00jihzkfgqzd0w5wn8bn"))))

(define qr.sh-script
  "#!/usr/bin/env sh
   size=15
   if [ \"$1\" == \"\" ]
   then
       qrencode -s $size -o - | feh -
   else
       echo \"$1\" | qrencode -s $size -o - | feh -
   fi")

(define-public qr.sh
  (package
	(name "rice-qr.sh")
	(version "1.0.1")
	(source (origin-file-dumper qr.sh-script))
	(build-system trivial-build-system)
	(arguments
	 `(#:modules
	   ((guix build utils))
	   #:builder
	   (begin
		 (use-modules (guix build utils))
		 (let* ((src (assoc-ref %build-inputs "source"))
				(path (string-append %output "/bin"))
				(dst (string-append path "/qr.sh")))
		   (mkdir-p path)
		   (copy-file src dst)
		   (chmod dst #o755)))))
	(propagated-inputs
	 `(("bash" ,bash)
	   ("qrencode" ,qrencode)
	   ("feh" ,feh)
	   ("coreutils" ,coreutils)))
	(home-page "https://terramorpha.tech/")
	(synopsis "the wallpaper for my rice")
	(description "asdfasdf")
	(license license:non-copyleft)))

(define-public compton.sh
  (let ((content
		 "#!/usr/bin/env sh
pkill -x compton
start-compton \\
	-r 20\\
	--backend glx\\
	--blur-background\\
	--blur-method kawase\\
	--blur-strength 10\\
	--detect-rounded-corners\\
	--config /dev/null\\
  	--shadow-exclude 'argb'\\
	--blur-background-exclude \"class_g = 'slop'\""))
	(package
	  (name "bspwm-compton")
	  (version "1.0.0")
	  (source (origin-file-dumper content))
	  (build-system trivial-build-system)
	  (arguments
	   `(#:modules
		 ((guix build utils))
		 #:builder
		 (begin
		   (use-modules (guix build utils))
		   (let* ((src (assoc-ref %build-inputs "source"))
				  (path (string-append %output "/bin"))
				  (dst (string-append path "/bspwm-start-compton.sh")))
			 (set-path-environment-variable "PATH"
											'("bin")
											(map cdr %build-inputs))

			 (mkdir-p path)
			 (copy-file src dst)
			 (substitute* dst
			   (("start-compton")
				(which "compton")))
			 (chmod dst #o755)))))
	  (propagated-inputs
	   `(("bash" ,bash)
		 ("coreutils" ,coreutils)))
	  (inputs
	   `(("compton" ,compton)))
	  (home-page "https://terramorpha.tech/")
	  (synopsis "the wallpaper for my rice")
	  (description "asdfasdf")
	  (license license:non-copyleft))))


(define-public keybinds-config
  (let* ((config-content (entries->string i3wm-keymap)))
	(package
	  (name "rice-i3-sxhkd-config")
	  (version "1.0.7")
	  (source (origin-file-dumper config-content))
	  (build-system trivial-build-system)
	  (arguments
	   `(#:modules
		 ((guix build utils))
		 #:builder
		 ,(builder-copy-source "/share/rice/sxhkd.conf")))
	
	  (home-page "https://terramorpha.tech/")
	  (synopsis "my keybindings")
	  (description "the sxhkd config file for my i3 setup")
	  (license license:non-copyleft))))

(define-public keybinds-script
  (package
   (name "rice-i3-sxhkd-script")
   (version "1.0.0")
   (source (origin-file-dumper ""))
   (build-system trivial-build-system)
   (arguments
	`(#:modules
	  ((guix build utils)
	   (terramorpha libs string-interpolation))
	  #:builder
	  (begin
		(use-modules (guix build utils)
					 (terramorpha libs string-interpolation))
		(let* ((path (string-append %output "/bin"))
			   (file (string-append path "/start-bindings.sh")))
		  (mkdir-p path)
		  (with-output-to-file file
			(lambda ()
			  (display
			   #<"#!/usr/bin/env sh
export SXHKD_SHELL=sh
killall -q sxhkd
sxhkd -c #((assoc-ref %build-inputs \"keybinds-config\"))/share/rice/sxhkd.conf
")))
		  (chmod file #o755)))))
   (inputs `(("keybinds-config" ,keybinds-config)))
   (propagated-inputs `(("psmisc" ,psmisc)))
   (home-page "https://terramorpha.tech/")
   (synopsis "a sxhkd script")
   (description
	"this script sets the correct environment variables and directs
sxhkd to the correct config file")
   (license license:non-copyleft)))

(define-public bspwm-sxhkd-config
  (let ((config-content (entries->string bspwm-keymap)))
	(package
	  (name "rice-bspwm-sxhkd-config")
	  (version "1.0.0")
	  (source (origin-file-dumper config-content))
	  (build-system trivial-build-system)
	  (arguments
	   `(#:modules
		 ((guix build utils)
		  (terramorpha libs string-interpolation))
		 #:builder
		 ,(builder-copy-source "/share/rice/sxhkd.conf")))
	  (home-page "https://terramorpha.tech/")
	  (synopsis "the bspwm sxhkd config")
	  (description "the config file for the bspwm iteration of my setup")
	  (license license:non-copyleft))))

(define bspwm-config-script
  "# this file was auto generated
screenlayout.sh
bspc monitor HDMI-0 -d 1 2 3 4 5 6 7 8 9 10
bspc monitor HDMI1 -d 11 12 13 14 15 16 17 18 19 20
bspc config pointer_modifier mod1
# bspc config click_to_focus none
bspc config focus_follows_pointer true
bspc config pointer_follows_monitor true
bspc rule -a *:*:slim state=floating layer=above
bspc rule -a *:*:calculator123 state=floating layer=above
bspc rule -a *:*:* layer=below
pkill bspwm_manage_events.sh
bspwm_manage_events.sh &
wm_start.sh &
dunst &
")

(define-public bspwm-bspwmrc
  (let ((config-content (entries->string bspwm-keymap)))
	(package
	  (name "rice-bspwmrc")
	  (version "1.0.0")
	  (source (origin-file-dumper ""))
	  (build-system trivial-build-system)
	  (arguments
	   `(#:modules
		 ((guix build utils)
		  (terramorpha libs string-interpolation))
		 #:builder
		 (begin
		   (use-modules (guix build utils)
						(terramorpha libs string-interpolation))
		   (let* ((path #<"#(%output)/bin")
				  (file #<"#(path)/bspwm-config.sh"))
			 (mkdir-p path)
			 (with-output-to-file file
			   (lambda ()
				 (display
				  (string-append
				   #<"#!/usr/bin/env sh
" ,bspwm-config-script #<"
export SXHKD_SHELL=sh
pkill sxhkd
sxhkd -c #((assoc-ref %build-inputs \"config-file\"))/share/rice/sxhkd.conf &
emacs --fg-daemon &
"))))
			 (chmod file #o755)))))
	  (inputs `(("config-file" ,bspwm-sxhkd-config)))
	  (propagated-inputs `(("procps" ,(specification->package "procps"))))
	  (home-page "https://terramorpha.tech/")
	  (synopsis "the bspwm script")
	  (description "the bspwm script that sets up all window rules and activates the sxhkd script")
	  (license license:non-copyleft))))

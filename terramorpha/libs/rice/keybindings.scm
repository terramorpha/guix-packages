#!curly-infix
(define-module (terramorpha libs rice keybindings)
  #:use-module (terramorpha libs string-interpolation)
  #:use-module (ice-9 match)

  #:export (bspwm-keymap
			i3wm-keymap))

(define-syntax match-map
  (syntax-rules ()
	((_ list pat exp)
	 (map (match-lambda (pat exp)) list))))


(define wm-generic-bindings
  (let ((mod 'super)
		(browser "qutebrowser")
		(terminal "termite"))
   `(,@(match-map `((f . ,browser)
					(c . "emacsclient -c --alternate-editor='emacs'"))
				  (key . program)
				  `({,mod + alt + ,key} ,program))
	
	 ({,mod + Print} "imagesaver.sh")
	 ({shift + Print} "screencapture.sh -s")
	 ({Print} "screencapture.sh -save -notify")
	 ({,mod + Return} ,terminal)
	 ({,mod + less} "termite -t slim")
	 ({,mod + d} "dmenu_run")
	 ({,mod + dead_cedilla} "calc.sh")

	 ;; sound
	 ,@(match-map '((XF86AudioRaiseVolume . "-i")
					(XF86AudioLowerVolume . "-d"))
				  (key . flag)
				  `({,key} ,#<"pamixer #(flag) 1" "notify-send -t 200 $(pamixer --get-volume)"))
	 ({XF86AudioMute} "pamixer -t")
	 ;; MPV
	 ({XF86AudioNext} "mpc -w next" "notify-send -t 2000 \"$(mpc current)\"")
	 ({XF86AudioPrev} "mpc -w prev" "notify-send -t 2000 \"$(mpc current)\"")
	 ({XF86AudioPlay} "mpc -w toggle")
	 ({XF86AudioStop} "notify-send -t 2000 \"$(mpc current)\""))))


(define bspwm-keymap
  (let ((mod 'super)
		(session-mod 'control)
		(browser "qutebrowser")
		(terminal "termite")
		(desktop-map '((1 . 1)
					   (2 . 2)
					   (3 . 3)
					   (4 . 4)
					   (5 . 5)
					   (6 . 6)
					   (7 . 7)
					   (8 . 8)
					   (9 . 9)
					   (0 . 10)
					   (F1 . 11)
					   (F2 . 12)
					   (F3 . 13)
					   (F4 . 14)
					   (F5 . 15)
					   (F6 . 16)
					   (F7 . 17)
					   (F8 . 18)
					   (F9 . 19)
					   (F10 . 20))))
	`(({,mod + f}
	   ;; "{ bspc node focused.!fullscreen -t fullscreen && polybar-msg cmd hide } || { bspc node focused.fullscreen -t tiled && polybar-msg cmd show; }"
	   "toggle_fullscreen.sh"
	   ;; "bspc node focused.!fullscreen -t fullscreen && polybar-msg cmd hide || bspc node focused.fullscreen -t tiled && polybar-msg cmd show"
	   )
	  ({,mod + a} "bspc node -c")
	  ,@(match-map '((Left . "west")
					 (Right . "east")
					 (Up . "north")
					 (Down . "south"))
				   (key . direction)
				   `({,mod + ,key} ,#<"bspc node -f #(direction)"))
	  ,@(match-map '((Left . "west")
					 (Right . "east")
					 (Up . "north")
					 (Down . "south"))
				   (key . direction)
				   `({,mod + shift + ,key} ,#<"bspc node -s #(direction) --follow"))
	  ({,mod + ,session-mod + r} "bspc ")
	  ({,mod + alt + space} "bspc node focused.!floating -t floating || bspc node focused.floating -t tiled")
	  ({,mod + Tab} "bspc node focused.tiled -f last.floating || bspc node focused.floating -f last.tiled")
	  ,@(match-map desktop-map
				   (key . desktop)
				   `({,mod + ,key} ,#<"bspc desktop -f #(desktop)"))
	  ,@(match-map desktop-map
				   (key . desktop)
				   `({,mod + shift + ,key} ,#<"bspc node -d #(desktop)"))

	  ,@(match-map `((f . "floating")
					 (t . "tiled")
					 (s . "fullscreen")
					 (p . "pseudo_tiled"))
				   (key . state)
				   `({{,mod + x} : ,key} ,#<"bspc node -t #(state)"))
	  ,@(match-map `((Left . "normal")
					 (Right . "normal")
					 (Up . "above")
					 (Down . "below"))
				   (key . layer)
				   `({{,mod + x} : ,key} ,#<"bspc node -l #(layer)")
				   )
	  ,@wm-generic-bindings)))

(define i3wm-keymap
  (let ((mod 'super)
		(session-mod 'control)
		(browser "qutebrowser")
		(term "termite"))


	`( ;; session
	  ({,mod + ,session-mod + c} "prompt.sh 'reload i3'")
	  ({,mod + ,session-mod + l} "lockscreen.sh")
	  ({,mod + ,session-mod + n} "prompt.sh 'power off' 'systemctl poweroff'")
	  ({,mod + ,session-mod + m} "prompt.sh 'suspend' 'systemctl suspend'")
	  ({,mod + ,session-mod + x} "prompt.sh 'exit i3' 'i3-msg exit'")
	  ({,mod + shift + Tab}      "i3-msg layout tabbed")
	  
	  ;; move window up down left right
	  ,@(match-map '((Left . "left")
					 (Right . "right")
					 (Up . "up")
					 (Down . "down"))
				   (key . name)
				   `({,mod + shift + ,key}
					 ,#<"i3-msg move #(name)"))
	  ;; focusing left or right
	  ,@(match-map '((Left . "left")
					 (Right . "right")
					 (Up . "up")
					 (Down . "down"))
				   (key . name)
				   `({,mod + ,key} ,#<"i3-msg focus #(name)"))
	  
	  ;; window manipulation
	  ({,mod + Tab} "i3-msg focus mode_toggle")
	  ({,mod + n} "i3-msg split h")
	  ({,mod + m} "i3-msg split v")
	  ({,mod + f} "i3-msg fullscreen toggle")
	  ({,mod + a} "i3-msg kill")
	  ({,mod + dead_cedilla} "calc.sh")
	  ({,mod + alt + space} "i3-msg floating toggle")
	  
	 ;; workspaces
	  ,@(match-map '(1 2 3 4 5 6 7 8 9)
				   v
				   `({,mod + ,v} ,#<"i3-msg workspace #(v)"))

	  ,@(match-map '(1 2 3 4 5 6 7 8 9)
				   v
				   `({,mod + shift + ,v} ,#<"i3-msg move container to workspace #(v); workspace #(v)"))
	  
	  ,@(match-map '(("F1" . 11)
					 ("F2" . 12)
					 ("F3" . 13)
					 ("F4" . 14)
					 ("F5" . 15)
					 ("F6" . 16)
					 ("F7" . 17)
					 ("F8" . 18)
					 ("F9" . 19))
				   (key . num)
				   `({,mod + ,key} ,#<"i3-msg workspace #(num)"))

	  ,@(match-map '(("F1" . 11)
					 ("F2" . 12)
					 ("F3" . 13)
					 ("F4" . 14)
					 ("F5" . 15)
					 ("F6" . 16)
					 ("F7" . 17)
					 ("F8" . 18)
					 ("F9" . 19))
				   (key . num)
				   `({,mod + shift + ,key} ,#<"i3-msg move container to workspace #(num); workspace #(num)"))
	  ,@wm-generic-bindings)))



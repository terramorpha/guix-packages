(define-module (terramorpha packages xmenu)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages image)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages base)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config))

(define version "3.4")
(define-public xmenu
  (package
    (name "xmenu")
    (version version)
    (source (origin
              (method git-fetch)
              (uri (git-reference
					(url "https://github.com/phillbush/xmenu")
					(commit (string-append "v" version))))
              (sha256
               (base32
                "1sw9l87fh03jp03a2v7rhgpyx29yg2x9blzfzp40jwad2whs7m7n"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils)
				  (rnrs io simple)
				  (ice-9 pretty-print))
       #:builder (begin
				   (use-modules (guix build utils)
								(rnrs io simple)
								(ice-9 pretty-print))
				   (let* ((input-paths (map cdr %build-inputs))
						  (pkg-config-path (append (map (lambda (name) (string-append name "/lib/pkgconfig"))
														input-paths)
												   (map (lambda (name) (string-append name "/share/pkgconfig"))
														input-paths)))
						  (path (map (lambda (name) (string-append name "/bin"))
									 input-paths))
						  (c-include-path (map (lambda (name) (string-append name "/include"))
											   input-paths))
						  (ld-library-path (map (lambda (name) (string-append name "/lib"))
												input-paths)))
					 (setenv "PKG_CONFIG_PATH" (string-join pkg-config-path ":"))
					 (setenv "PATH" (string-join path ":"))
					 (setenv "C_INCLUDE_PATH" (string-join c-include-path ":"))
					 (setenv "LIBRARY_PATH" (string-join ld-library-path ":"))
					 (copy-recursively (assoc-ref %build-inputs "source") ".")
					 (delete-file "Makefile")
					 (call-with-output-file "Makefile"
					   (lambda (file)
						 (format file "INCS = $(shell pkg-config --cflags freetype2 fontconfig x11 imlib2 xft)
LIBS = $(shell pkg-config --libs freetype2 fontconfig x11 imlib2 xft)
CFLAGS = -Wall -Wextra ${INCS}
LDFLAGS = ${LIBS}
OUTDIR=~a
DOCDIR=~a
CC = gcc
PROG = xmenu
SRCS = ${PROG}.c
OBJS = ${SRCS:.c=.o}
all: ${PROG}
${PROG}: ${OBJS}
~/echo \"test\"
~/echo ${LIBS}
~/${CC} -o $@ ${OBJS} ${LDFLAGS}
${OBJS}: config.h
.c.o:
~/${CC} ${CFLAGS} -c $<
clean:
~/rm ${OBJS} ${PROG}
install: all
~/install -D -m 755 ${PROG} ${OUTDIR}/bin/${PROG}
~/install -D -m 644 ${PROG}.1 ${DOCDIR}/share/man/man1/${PROG}.1
"
								 ;;".PHONY: all clean install uninstall"
								 %output
								 (assoc-ref %outputs "doc"))))
					 (system* "make" "install")))))
    (native-inputs `(("imlib2" ,imlib2)
					 ("fontconfig" ,fontconfig)
					 ,@(package-inputs fontconfig)
					 ("libx11" ,libx11)
					 ,@(package-inputs libx11)
					 ("libxft" ,libxft)
					 ("coreutils" ,coreutils)
					 ("gcc-toolchain" ,gcc-toolchain)
					 ("make" ,gnu-make)
					 ("source" ,source)
					 ("pkg-config" ,pkg-config)
					 ("zlib" ,zlib)
					 ("glibc" ,glibc)))
	(outputs '("out"
			   "doc"))
    (home-page "https://github.com/phillbush/xmenu")
    (synopsis "XMenu is a menu utility for X.")
    (description "XMenu receives a menu specification in stdin, shows a menu for the user
to select one of the options, and outputs the option selected to stdout.
XMenu can be controlled both via mouse and via keyboard.  The menu is a
pop-up menu (that is, after selecting an option, the menu disappears).")
    (license license:non-copyleft)))

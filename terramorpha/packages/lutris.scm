(define-module (terramorpha packages lutris)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages glib)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages compression))

(define-public lutris
  (package
   (name "lutris")
   (version "0.5.7.1")
   (source (origin
            (method
			 ;;url-fetch
			 git-fetch)
            (uri
			 (git-reference
			  
			  (url "https://github.com/lutris/lutris.git")
			  (commit "v0.5.7.1")))
            (sha256
             (base32
              "12ispwkbbm5aq263n3bdjmjfkpwplizacnqs2c0wnag4zj4kpm29"))))
   (build-system python-build-system)
   (arguments `(#:tests? #f))
   (inputs
	`(("python-requests" ,python-requests)
	  ("python-evdev" ,python-evdev)
	  ("python-pygobject" ,python-pygobject)
	  ("python-pyyaml" ,python-pyyaml)))
   (propagated-inputs
	`(("gtk3" ,gtk+)
	  ("glib" ,glib)))
   
   (home-page "https://lutris.net")
   (synopsis "An open gaming platform")
   (description "whatever")
   (license license:silofl1.1)))

(define-module (terramorpha packages emacsclient-desktop)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (gnu packages emacs)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:))

(define-public font-spectral-ttf
  (package
   (name "emacsclient-desktop")
   (version "1.0.0")
   (source (plain-file "empty-file" ""))
   (build-system trivial-build-system)
   (arguments
    `(#:modules
	  ((guix build utils))
	  #:builder
	  (begin
		(use-modules (guix build utils))
		(mkdir-p (string-append %output "/share/applications"))
		(with-output-to-file (string-append %output "/share/applications/emacsclient.desktop")
		  (lambda ()
			(display (string-append "[Desktop Entry]
Name=Emacs Client
GenericName=Text Editor
Comment=Edit text
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
Exec=" (assoc-ref %build-inputs "emacs") "/bin/emacsclient" " -nc --alternate-editor='' %F
Icon=emacs
Type=Application
Terminal=false
Categories=Development;TextEditor;
StartupWMClass=Emacs
Keywords=Text;Editor;
")))))))
   (inputs `(("emacs" ,emacs)))
   (home-page "none")
   (synopsis "A desktop file for emacsclient")
   (description "A desktop file for emacsclient")
   (license license:gpl2)))

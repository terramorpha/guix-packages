(define-module (terramorpha packages typescript)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages node)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages base))

(define-public typescript
  (package
   (name "typescript")
   (version "4.0.2")
   (source (origin
			(method url-fetch)
			(uri (string-append "https://registry.npmjs.org/" name "/-/" name "-" version ".tgz"))
			(sha256 (base32 "1sxg37fizfm7h6405nk6akzs0i6s3vw87qysmx7myqz32g2m6rkm"))))

   (build-system trivial-build-system)
   (arguments
	`(
	  #:modules ((guix build utils))
				#:builder
				(begin
				  (use-modules (guix build utils))
				  (display (map cdr %build-inputs)) (newline)
				  (set-path-environment-variable "PATH"
												 '("bin")
												 (map cdr %build-inputs))
				  (setenv "HOME" "./")
				  (system* "npm" "install" "-g" "--user" "root" "--prefix" %output (assoc-ref %build-inputs "source"))



				  
				  ;; (system* "tar" "-xf" (assoc-ref %build-inputs "source"))
				  ;; (system "ls -a")
				  ;; (chdir "./package")
				  ;; (system "ls -a")
				  ;; (display (environ)) (newline)
				  ;; (display %build-inputs) (newline)
				  

				  )))
   (propagated-inputs `(("node" ,node)))
   ;; (native-inputs `(("tar" ,tar)
   ;; 				 ("tar" ,gzip)
   ;; 				 ("coreutils" ,coreutils))
   ;;  )
   (synopsis "typescript")
   (description "typescript")
   (license asl2.0)
   (home-page "https://www.typescriptlang.org/")))



;; # Maintainer: Felix Yan <felixonmars@archlinux.org>
;; # Contributor: Bruno Galeotti <bgaleotti at gmail dot com>

;; pkgname=typescript
;; pkgver=4.0.2
;; pkgrel=1
;; pkgdesc="TypeScript is a language for application scale JavaScript development"
;; arch=('any')
;; url="http://typescriptlang.org/"
;; license=('Apache')
;; depends=('nodejs')
;; makedepends=('npm')
;; source=(https://registry.npmjs.org/$pkgname/-/$pkgname-$pkgver.tgz)
;; noextract=($pkgname-$pkgver.tgz)
;; sha512sums=('7b8111bd1576c1bfab459fc841e6f78e6d95c41b22ad02e9421771a65676304cc6bc39243263e095e7273437d25018af3233fddef45b9e06180d0a90ffbf1b71')

;; package() {
;;   npm install -g --user root --prefix "$pkgdir"/usr "$srcdir"/$pkgname-$pkgver.tgz

;;   # Non-deterministic race in npm gives 777 permissions to random directories.
;;   # See https://github.com/npm/npm/issues/9359 for details.
;;   chmod -R u=rwX,go=rX "$pkgdir"

;;   # npm installs package.json owned by build user
;;   # https://bugs.archlinux.org/task/63396
;;   chown -R root:root "$pkgdir"
;; }

;; # vim:set ts=2 sw=2 et:

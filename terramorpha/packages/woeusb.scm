(define-module (terramorpha packages woeusb)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages wget)
  #:export (woeusb))

(define woeusb
  (package
   (name "woeusb")
   (version "5.0.4")

   (source (origin
			(method git-fetch)
			(uri (git-reference (url "https://github.com/WoeUSB/WoeUSB.git")
								(commit (string-append "v" version))))
			(sha256 (base32 "0a2386jzi9la70jdyii7vxh0s7pdph2cirj671h3kcjaf9jfcri2"))))
   (license gpl3+)
   (build-system trivial-build-system)
   (arguments
	`(#:modules ((guix build utils))
	  #:builder (begin
				  (use-modules (guix build utils))
				  (chdir (assoc-ref %build-inputs "source"))
				  (mkdir-p (string-append %output "/sbin"))
				  (mkdir-p (string-append %output "/share/man/man1"))
				  (copy-file "share/man/man1/woeusb.1" (string-append %output "/share/man/man1/woeusb.1"))
				  (copy-file "sbin/woeusb" (string-append %output "/sbin/woeusb"))
				  #t)))
   (synopsis "A Microsoft Windows® USB installation media preparer for GNU+Linux")
   (description "A Microsoft Windows® USB installation media preparer for GNU+Linux")
   (home-page "https://github.com/WoeUSB/WoeUSB")
   (native-inputs
	`(("source" ,source)))
   (propagated-inputs
	`(("grub" ,grub)
	  ("ncurses" ,ncurses)
	  ("bash" ,bash)
	  ("ntfs-3g" ,ntfs-3g)
	  ("dosfstools" ,dosfstools)
	  ("wget" ,wget)
	  ("parted" ,parted)))))

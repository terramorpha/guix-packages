(define-module (terramorpha packages zig))

(use-modules (guix packages))
(use-modules (guix git-download))
(use-modules (guix build-system cmake))
(use-modules ((guix licenses) #:prefix license:))


(use-modules (gnu packages)
			 (gnu packages base)
			 (gnu packages cmake)
			 (gnu packages llvm))

(define-public zig
  (package
	(name "zig")
	(version "0.6.0")
	(source (origin
			  (method git-fetch)
			  
			  (uri
			   (git-reference
				(url "https://github.com/ziglang/zig.git")
				(commit "7c2bde1f07f5e672bb2320c517568dab9edab652")))
			  (sha256
			   (base32
				"1id4766nw2cy2sbfs5q1ggf61mjhxl1xvl5prf7mr7qxyvf0pk4j"))))
	(build-system cmake-build-system)

	(home-page "https://ziglang.org/")
	(synopsis "zig language compiler")
	(native-inputs `(("llvm" ,(specification->package "llvm@11"))
					 ("clang" ,(specification->package "clang@11"))
					 ("lld" ,(specification->package "lld@11"))))
	(arguments
	 
	 `(#:tests?
	   #f
	   #:phases
	   (cons (cons 'set-home-env
				   (lambda _
					 ;;(display (getcwd)) (newline)
					 (setenv "HOME" (getcwd)))) %standard-phases)))
	(description "Zig is a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software.")
	(license license:non-copyleft)))

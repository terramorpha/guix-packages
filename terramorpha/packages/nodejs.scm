(define-module (terramorpha packages nodejs)
  #:use-module (guix packages)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages base)
  #:use-module (gnu packages python)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public nodejs
  (package
   (name "nodejs")
   (version "14.0.0")
   (source (origin
			;; (method url-fetch)
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/nodejs/node.git")
				  (commit (string-append "v" version))))
			(sha256 (base32 "1jaszbab2a0pm54harmr5f0v4i81d0wm316vmfihs40wphi8ckkn"))))
   (arguments `(#:configure-flags
				'("--salut")
				#:tests?
				#f				  
				#:phases (modify-phases %standard-phases
						   (add-before 'configure 'fix-/bin/sh
									   (lambda _
										 (display (getenv "PATH"))
										 (substitute* "./configure"
										   (("'/bin/sh'") (string-append "'" (which "sh") "'")))))
						   (replace 'configure
									(lambda* (#:key build #:allow-other-keys)
									  (setenv "CC" (which "gcc"))
									  (setenv "CXX" (which "g++"))
									  (system* "cat" "./configure")
									  (let ((res (system* "./configure"
														  ;; (string-append "CONFIG_SHELL=" (which "bash"))
														  ;; (string-append "SHELL=" (which "bash"))

														  (string-append "--prefix=" %output)
														  ;;(string-append "--build=" build)
														  )))
										(display res) (newline))
									  #t)))))
   (build-system gnu-build-system)
   ;; (inputs `(("libpcap" ,libpcap)
   ;; 			 ("libnet" ,libnet)))
   (native-inputs `(("python3" ,python)
					("python2" ,python-2.7)
					("which" ,which)))
   (synopsis "node")
   
   (description
	"nodejs")
   
   (home-page "https://github.com/nodejs/node.git")
   (license license:non-copyleft)))

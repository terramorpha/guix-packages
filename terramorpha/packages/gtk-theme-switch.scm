(define-module (terramorpha packages gtk-theme-switch)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gtk)
  #:export (gtk-theme-switch))



(define gtk-theme-switch
  (package
	(name "gtk-theme-switch")
	(version "2.1.0")
	(source (origin
			  (uri (string-append "http://ftp.de.debian.org/debian/pool/main/g/gtk-theme-switch/gtk-theme-switch_" version ".orig.tar.gz"))
			  (method url-fetch)
			  (sha256 (base32 "00cpvimachir6sqj8gw48h6vazl7whbg60h87bpffir4p4msx7sf"))
			  ))
	(arguments
	 `(
	   #:phases (modify-phases %standard-phases
				  (delete 'configure)
				  (delete 'check)
				  (replace 'install (lambda* (#:key outputs #:allow-other-keys)
									  ;; (use-modules (guix build utils))
									  (install-file "gtk-theme-switch2" (string-append (assoc-ref outputs "out") "/bin"))
									  (install-file "gtk-theme-switch2.1" (string-append (assoc-ref outputs "out") "/share/man/man1"))
									  
									  )))))
	(build-system gnu-build-system)
	(native-inputs `(("pkg-config" ,pkg-config)))
	(inputs `(("gtk+-2" ,(specification->package "gtk+@2"))))
	(synopsis "A simple gtk2 theme switcher")
	(description "A program used to select the gtk2 theme")
	(license gpl2)
	(home-page "http://muhri.net/nav.php3?node=gts")
	))

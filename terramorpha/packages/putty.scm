(define-module (terramorpha packages putty)
  #:use-module (guix packages)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config))

(define-public putty
  (package
   (name "putty")
   (version "0.74.0")
   (source (origin
			;; (method url-fetch)
			(method url-fetch)
			(uri "http://www.putty.be/0.74/putty-0.74.tar.gz")
			(sha256 (base32 "0zc43g8ycyf712cdrja4k8ih5s3agw1k0nq0jkifdn8xwn4d7mfx"))))
   (arguments `(#:tests? #f
				#:phases (modify-phases %standard-phases
							  (add-before 'configure 'go-into-unix/
										  (lambda _ (begin
												 (chdir "unix")
												 #t))))))
   (build-system gnu-build-system)
   (inputs `(("gtk+" ,gtk+)))
   (native-inputs `(("perl" ,perl)
					("python" ,python)
					("python2" ,python-2.7)
					("pkg-config" ,pkg-config)))
   (synopsis "A network address discovery tool")
   (description
	"Putty")

   (home-page "https://www.chiark.greenend.org.uk/~sgtatham/putty/")
   (license  (license:non-copyleft
			  "https://www.chiark.greenend.org.uk/~sgtatham/putty/licence.html"
			  "The putty license"))))

(define-module (terramorpha packages plasma-desktop)
  #:use-module (guix packages)
  ;; #:use-module (guix download)
  #:use-module (guix git-download)
  ;; #:use-module (guix build-system gnu)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages qt)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))



;; (define-public hello)
(define-public plasma
  (package
   (name "plasma")
   (version "5.19.0")
   (source (origin
			;; (method url-fetch)
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/KDE/plasma-desktop.git")
				  (commit "v5.19.0")))
			(sha256 (base32 "13jacrfbasbwx264alrf8n6iam5ls6xlfxjbmn0s7m359bsvg5xz"))))
   ;; (build-system gnu-build-system)

   (build-system cmake-build-system)
   ;; (arguments '(#:configure-flags '("--enable-silent-rules")))
   (native-inputs `(("extra-cmake-modules" ,extra-cmake-modules)
					("qtbase" ,qtbase)
					("qtdeclarative" ,qtdeclarative)
					("qtx11extras" ,qtx11extras)
					("kcmutils" ,kcmutils)
					("qtsvg" ,qtsvg)
					("kdoctools" ,kdoctools)
					("ki18n" ,ki18n)
					("knewstuff" ,knewstuff)
					("knotifications" ,knotifications)
					("kactivities" ,kactivities) ("kactivities-stats" ,kactivities-stats)
					("kwallet" ,kwallet)
					("kdeclarative" ,kdeclarative)
					("knotifyconfig" ,knotifyconfig)
					("kdelibs4support" ,kdelibs4support)
					("krunner" ,krunner)
					("kglobalaccel" ,kglobalaccel)
					("kirigami" ,kirigami)
					("qqc2-desktop-style" ,qqc2-desktop-style)
					("plasma-framework" ,plasma-framework)))

   (synopsis "kde window manager")
   (description "asdfasdf")
   (home-page "kde")
   ;; c'est apache-2

   (license gpl3+)))





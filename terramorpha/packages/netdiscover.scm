(define-module (terramorpha packages netdiscover)
  #:use-module (guix packages)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public netdiscover
  (package
   (name "netdiscover")
   (version "0.7")
   (source (origin
			;; (method url-fetch)
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/netdiscover-scanner/netdiscover")
				  (commit version)))
			(sha256 (base32 "0g8w8rlg16dsibxi4dnyn7v7r8wwi5ypd51c4w59j0ps2id0w8yj"))))
   (arguments `(#:tests? #f))
   (build-system gnu-build-system)
   (inputs `(("libpcap" ,libpcap)
			 ("libnet" ,libnet)))
   (native-inputs `(("automake" ,automake)
					("autoconf" ,autoconf)))
   (synopsis "A network address discovery tool")

   (description
	"Netdiscover is a network address discovering tool, developed mainly for those
wireless networks without dhcp server, it also works on hub/switched networks.
Its based on arp packets, it will send arp requests and sniff for replies.")
   
   (home-page "https://github.com/netdiscover-scanner/netdiscover")
   (license license:gpl3)))

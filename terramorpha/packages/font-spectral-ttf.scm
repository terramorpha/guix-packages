(define-module (terramorpha packages font-spectral-ttf)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages compression))

(define font-spectral-commit "68414e1f632007dd6f51d1fa45bc365881aa2e2b")
(define-public font-spectral-ttf
  (package
   (name "font-spectral-ttf")
   (version font-spectral-commit)
   (source (origin
            (method
			 ;;url-fetch
			 git-fetch)
            (uri
			 (git-reference
			  (url "https://github.com/productiontype/Spectral")
			  (commit font-spectral-commit)))
            (sha256
             (base32
              "0mddvs1jcr61r87jzxajv3w3fs7n5nrqffvl6cxj1hc9hdpqw9gg"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules
	  ((guix build utils)
	   (ice-9 ftw))
	  #:builder
	  (begin
		(use-modules (guix build utils)
					 ((ice-9 ftw ) #:select (scandir)))
		;; (set-path-environment-variable "PATH" "/bin" (map cdr %build-inputs))
		;; (system "./")

		(let ((out (string-append %output "/share/fonts/truetype/"))
			  (pwd (getcwd))
			  (src (string-append (assoc-ref %build-inputs "source")
								  "/fonts/desktop")))
		  (mkdir-p out)
		  (for-each (lambda (filename)
					  (if (string-suffix? ".ttf" filename)
						  (copy-file (string-append src "/" filename) (string-append out filename))))
					(scandir src))))))
   ;; (native-inputs `(("source" ,source)
   ;; 					("sassc" ,sassc)
   ;; 					("optipng" ,optipng)
   ;; 					("inkscape" ,inkscape)
   ;; 					("glib" ,glib)))
   (home-page "whatever")
   (synopsis "a theme")
   (description "whatever")
   (license license:silofl1.1)))

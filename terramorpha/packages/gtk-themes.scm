(define-module (terramorpha packages gtk-themes)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages web)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages image)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages glib))

(define-public whitesur-gtk-theme
  (let ((commit "79bec676d330c469eee0eec879ac6a2b4b46c07e"))
	(package
	 (name "whitesur-gtk-theme")
	 (version commit)
	 (source (origin
			  (method
			   git-fetch)
			  (uri
			   (git-reference
				(url "https://github.com/vinceliuice/WhiteSur-gtk-theme.git")
				(commit commit)))
			  (sha256
			   (base32
				"1m3xxzg0454vq65q0vmw645qg4figxrfyb6574v0arad9s2ddkgn"))))
	 (build-system trivial-build-system)
	 (arguments
	  `(#:modules
		((guix build utils))
		#:builder
		(begin
		  (use-modules (guix build utils))
		  (set-path-environment-variable "PATH" '("bin") (map cdr %build-inputs))
		  (copy-recursively (assoc-ref %build-inputs "source") ".")
		  (patch-shebang "./install.sh")
		  (system* "./install.sh" "-d" (string-append %output "/share/themes"))
		  #t)))
	 (native-inputs `(("source" ,source)
					  ("sassc" ,sassc)
					  ("optipng" ,optipng)
					  ("inkscape" ,inkscape)
					  ("glib" ,glib)
					  ("bash" ,bash)
					  ("coreutils" ,coreutils)
					  ("glib" ,glib "bin")))
	 (home-page "https://github.com/vinceliuice/WhiteSur-gtk-theme")
	 (synopsis "WhiteSur Gtk Theme")
	 (description "WhiteSur is a MacOS Big Sur like theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.")
	 (license gpl3))))

(define-public whitesur-icon-theme
  (let ((commit "e4f9697ac3b86a8caaf0d7835e9047fc66fb2462"))
	(package
	 (name "whitesur-icon-theme")
	 (version commit)
	 (source (origin
			  (method
			   git-fetch)
			  (uri
			   (git-reference
				(url "https://github.com/vinceliuice/WhiteSur-icon-theme.git")
				(commit commit)))
			  (sha256
			   (base32
				"1az7aa3p0knc9kjhk9bqpl9x230b4ygav4fb4pr3wccikxvy61h4"))))
	 (build-system trivial-build-system)
	 (arguments
	  `(#:modules
		((guix build utils))
		#:builder
		(begin
		  (use-modules (guix build utils))
		  (set-path-environment-variable "PATH" '("bin") (map cdr %build-inputs))
		  (copy-recursively (assoc-ref %build-inputs "source") ".")
		  (substitute* "install.sh" (("#!/bin/bash") (string-append (assoc-ref %build-inputs "bash") "/bin/bash")))
		  (setenv "HOME" (string-append (getcwd) "/home"))
		  (mkdir "home")
		  (let ((dst (string-append %output "/share/icons")))
			;; (patch-shebang "./install.sh")



			(mkdir-p dst)
			(display (system* "./install.sh" "-d" dst)))
		  #t)))
	 (native-inputs `(("source" ,source)
					  ;; ("sassc" ,sassc)
					  ;; ("optipng" ,optipng)
					  ;; ("inkscape" ,inkscape)
					  ;; ("glib" ,glib)
					  ("bash" ,bash)
					  ("coreutils" ,coreutils)
					  ;; ("glib" ,glib "bin")
					  ))
	 (home-page "https://github.com/vinceliuice/WhiteSur-gtk-theme")
	 (synopsis "WhiteSur Gtk Theme")
	 (description "WhiteSur is a MacOS Big Sur like theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.")
	 (license gpl3))))

(define-public whitesur-cursors-theme
  (let ((commit "1ada17d4e0ad96f5401f5d0f2d917a152f62c556"))
	(package
	 (name "whitesur-cursors-theme")
	 (version commit)
	 (source (origin
			  (method
			   git-fetch)
			  (uri
			   (git-reference
				(url "https://github.com/vinceliuice/WhiteSur-cursors.git")
				(commit commit)))
			  (sha256
			   (base32
				"1jzvq2h417ql2chdfy1c4nc4cprc9s9cw19kbxpiy8sqnsc3j5d1"))))
	 (build-system trivial-build-system)
	 (arguments
	  `(#:modules
		((guix build utils))
		#:builder
		(begin
		  (use-modules (guix build utils))
		  (let ((out (string-append %output "/share/icons")))
			(mkdir-p out)
			(copy-recursively (string-append (assoc-ref %build-inputs "source") "/dist") (string-append out "/WhiteSur-cursors")))
		  #t)))
	 (home-page "https://github.com/vinceliuice/WhiteSur-gtk-theme")
	 (synopsis "WhiteSur Gtk Theme")
	 (description "WhiteSur is a MacOS Big Sur like theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Pantheon, XFCE, Mate, etc.")
	 (license gpl3))))

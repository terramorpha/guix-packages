(define-module (terramorpha packages emacs-pgtk)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix build utils) #:select (modify-phases))
  #:use-module (guix git-download)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gtk))

(define-public emacs-pgtk
  (package
    (inherit emacs-next)
    (name "emacs-pgtk")
	(source (origin (method git-fetch)
					(uri (git-reference (url "https://github.com/fejfighter/emacs.git")
										(commit "c63dc6de7f38d613f15e968221436460693ac765")))
					(sha256 (base32 "00rlgxkc0pwnl7n59snx8dnflpbw2df8zdllcbwj3m648a24dk16"))))
    (synopsis "The extensible, customizable, self-documenting text
editor (pure gtk fork)")
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags ''())
        `(cons "--with-pgtk" ,flags))
	   ((#:phases phases ''())
		`(modify-phases ,phases
		   (delete 'strip-double-wrap)))))
    (inputs
     `(("gtk" ,gtk+)
       ,@(package-inputs emacs)))))

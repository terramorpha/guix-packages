(define-module (terramorpha packages make-guile)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (gnu packages base)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils))

(define-public make-guile
  (package
    (inherit gnu-make)
    (name "make-guile")
    (build-system gnu-build-system)
    (arguments
     (substitute-keyword-arguments (package-arguments gnu-make)
       ((#:configure-flags flags ''())
        `(cons "--with-guile" ,flags))))))

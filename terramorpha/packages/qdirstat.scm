;; qmake INSTALL_PREFIX=/usr/local
;; https://github.com/shundhammer/qdirstat.git

(define-module (terramorpha packages qdirstat)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  ;; #:use-module (gnu packages networking)
  ;; #:use-module (gnu packages admin)
  ;; #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages compression)
  #:use-module ((guix licenses) #:prefix license:))

(define-public qdirstat
  (package
   (name "qdirstat")
   (version "1.7")
   (source (origin
			;; (method url-fetch)
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/shundhammer/qdirstat.git")
				  (commit version)))
			(sha256 (base32 "163x3fxra0l3vvrzm25mh7jvcwjbmwsqlpppkxx76mkz9a1769fy"))))
   (arguments
	`(#:phases (modify-phases %standard-phases
				 (add-after 'configure 'qmake
				   (lambda* (#:key outputs #:allow-other-keys)
					 (system* "qmake" (string-append "INSTALL_PREFIX=" (assoc-ref outputs "out")))
					 #t))
				 (delete 'configure))))
   (build-system gnu-build-system)
   (inputs `(("qtbase" ,qtbase)
			 ("zlib" ,zlib)))
   (synopsis "A storage utilisation vizualization tool")
   (description
	"QDirStat is a graphical application to show where your disk space has gone and to help you to clean it up.")
   
   (home-page "https://github.com/shundhammer/qdirstat")
   (license license:gpl2)))


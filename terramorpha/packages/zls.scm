(define-module (terramorpha packages zls))

(use-modules (guix packages))
(use-modules (guix git-download))
(use-modules (guix build-system trivial))
(use-modules ((guix licenses) #:prefix license:))


(use-modules (gnu packages)
			 (gnu packages base)
			 (gnu packages cmake)
			 (gnu packages llvm)
			 (terramorpha packages zig))

(define-public zls
  (package
	(name "zls")
	(version "0.0.1")
	(source (origin
			  (method git-fetch)
			  (uri
			   (git-reference
				(url "https://github.com/zigtools/zls.git")
				(commit "e8c20351d85da8eb4bf22480045b994007284d69")
				(recursive? #t)))
			  (sha256
			   (base32
				"06g8gml1g0fmvcfysy93bd1hb64vjd2v12x3kgxz58kmk5z0168y"))))
	(build-system trivial-build-system)
	(arguments
	 `(#:modules ((guix build utils))
	   #:builder
	   (begin
		 (use-modules (guix build utils))
		 (setenv "HOME" (getcwd))
		 (copy-recursively (assoc-ref %build-inputs "source") ".")
		 (set-path-environment-variable "PATH"
										'("bin")
										(map cdr %build-inputs))
		 (display (getcwd)) (newline)
		 (system* "zig" "build")
		 (install-file "zig-cache/bin/zls" (string-append %output "/bin")))))
	(home-page "https://github.com/zigtools/zls")
	(synopsis "a language server protocol implementation for the zig programming language")
	(description "Zig is a general-purpose programming language and toolchain for maintaining robust, optimal, and reusable software.")
	(native-inputs `(("zig" ,zig)))
	(license license:non-copyleft)))

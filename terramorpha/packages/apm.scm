(define-module (terramorpha packages apm)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages node)
  ;; #:use-module (gnu packages compression)
  ;; #:use-module (gnu packages base)
  )

(define-public apm
  (package
	(name "apm")
	(version "2.6.0")
	(source (origin
			  (method git-fetch)
			  (uri
			   (git-reference
				(url "https://github.com/atom/apm.git")
				(commit (string-append "v" version))))
			  (sha256 (base32 "04flid3359kcjc41q05v8cvis6g0qz442l3vpjl4bzn6fa8s84df"))))

	(build-system trivial-build-system)
	(arguments
	 `(
	   #:modules ((guix build utils))
	   #:builder
	   (begin
		 (use-modules (guix build utils))
		 (display (map cdr %build-inputs)) (newline)
		 (set-path-environment-variable "PATH"
										'("bin")
										(map cdr %build-inputs))
		 (setenv "HOME" "./")
		 (system* "npm" "install" "-g" "--user" "root" "--prefix" %output (assoc-ref %build-inputs "source"))



		 
		 ;; (system* "tar" "-xf" (assoc-ref %build-inputs "source"))
		 ;; (system "ls -a")
		 ;; (chdir "./package")
		 ;; (system "ls -a")
		 ;; (display (environ)) (newline)
		 ;; (display %build-inputs) (newline)
		 

		 )
	   ))
	(propagated-inputs `(("node" ,node)))
	;; (native-inputs `(("tar" ,tar)
	;; 				 ("tar" ,gzip)
	;; 				 ("coreutils" ,coreutils))
	;;  )
	(synopsis "atom package manager")
	(description "the package manager for atom")
	(license non-copyleft)
	(home-page "https://github.com/atom/apm")))


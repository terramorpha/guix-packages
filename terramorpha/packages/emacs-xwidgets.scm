(define-module (terramorpha packages emacs-xwidgets)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gnome)
    #:use-module (gnu packages tls))

(define-public emacs-xwidgets
  (package
    (inherit emacs)
    (name "emacs-xwidgets")
    (synopsis "The extensible, customizable, self-documenting text
editor (with xwidgets support)")
    (build-system gnu-build-system)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags ''())
        `(cons "--with-xwidgets" ,flags))))
    (inputs
     `(("webkitgtk" ,webkitgtk)
       ("libxcomposite" ,libxcomposite)
       ,@(package-inputs emacs)))
	(propagated-inputs
	 `(("glib-networking" ,glib-networking)
	   ("gnutls" ,gnutls)))))
